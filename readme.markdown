JLee
=====

JLee is an ant build system setup for fast agile android java application 
development. First part of it will be just individual scripts to generate 
codeqa/javadocs, run cucumber and javamonkey tests with later versions
integrating the whole thing for use in continuous build integration servers.